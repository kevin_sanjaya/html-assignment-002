# Scrum style #

## Epic: ##

### As a Product Owner ###
### I want to create an image gallery ###
### So our customers can navigate through their memories ###

* The customer should be able to see the large images in the main content section of the gallery.
* The customer should be able to navigate to the next image just clicking a button.
* The customer should be able to navigate to the previous image just clicking a button.

------

## User Story: ##

### As an user ###
### I want to see only one image at a time ###
### And navigate through my memories with a press of a button ###



------

## User Story: ##

### As an user ###
### I want to be able to see all the memories at a glance but using thumbnails  ###
### And when clicking on one of the thumbnails that image should be shown ###
### So I can decide at a glance what to see ###


------

## User Story: ##

### As an user ###
### I want to see at a glance what thumbnail is currently selected ###
### So I can see what image is going to be loaded even if I have to wait for the real size image to be loaded ###


------

## User Story: ##

### As an user ###
### I want to be able to see my memories in desktop and mobile devices including tablets ###
### So I can see my memories from anywhere ###
